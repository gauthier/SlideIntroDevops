
# Intégration et déploiement continue avec Gitlab

### Ou comment mettre en œuvre DevOps

---

## Les préconisations de DevOps

----

### Culture collaborative, procédés et outils, mesure

* Casser les silots
* Utiliser des outils pour accélérer les procédés et diminuer les risques d'erreurs humaines.

----

### Plus petit, plus vite et plus souvent

Travailler sur les développements petits (micro-services), les installer en production plus vite.

----

### Des équipes intégrées multicompétentes et autonomes

Modifier l'organisation de la DSI autour d'équipes autonomes. multicompétentes, en charge de l'ensemble du cycle de vie d'une release applicative.

----

### Une architecture agile

Agilité de l'architecture du système d'information. Pour que les équipes soient autonomes et puissent conduire à leur rythme les mise en production.

----

### La sureté de fonctionnement des plateformes

Qualité des tests. Déploiement et activation progressive des fonctionnalités pour limiter les risques inhérents à toute modification du système opéré en production.

---

## Développement  continu

----

### Scrum

* Choix en début de sprints des fonctionnalités à développer.
* Brièveté des itérations pour livrer rapidement.
* Nombre de fonctionnalités developpées par sprint limité pour augmenter la vélocité

----

### Kanban

* Permet une remise en cause du périmètre par rapport à Scrum.
* Pas de sprints mais un nombre de developpement en parallèle limité.

---

## DevOps et intégration continue

----

### Le développement sans branche de l'intégration continue

* Tout le monde publie dans la même branche.
* Compilation et test à chaque publication (push)

----

### Les branches par abstraction

Intégrer directement du code "mort", activable à la demande pour un groupe d'utilisateurs précis.

----

### Gaspillages de l'intégration tardive et les branches

* Branche par fonctionnalité
* Branche par équipe
* Branche par release

---

## Intégration continue

L'intégration continue est un **ensemble de pratiques** utilisées en génie logiciel consistant à **vérifier à chaque modification de code** source que le résultat des modifications ne produit **pas de régression** dans l'application développée.

----

### Intégration continue

* Compilation
* tests unitaires et fonctionnels
* tests qualité/sécurité
* tests de performances
* tests de non régression
* vérifier la couverture des tests
* tests d'intégration

----

### Intégration continue

* Les tests ne sont pas une variable d'ajustement du planing.
* Les developpeurs testent leur code en continu.
* Les tests se font en parallèle.
* L'automatisation des builds et les tests unitaires valide les changements.

#### Résultat

* Bugs trouvés et corrigés plus rapidement.
* Productivité amélioré grace à l'automatisation.

----

### Loi du changement continu et de l'augmentation de la complexité.

 * Any software system used in the real-world must change or become less and less useful in the environment.
 * As a system evolves, its complexity increases unless work is done to maintain or reduce it.

Manny Lehman 1974

---

## Déploiement continu

* Seul les logiciels déployés créent de la valeur.
* Plus fréquents, plus petits, moins risqués.
* Pour accélérer le déploiement et accélérer le retour utilisateur.
* Avec le déploiement continue le code est déployé dès qu'il est prêt.

----

### En quoi consiste un déploiement

* Automatisation des déploiements (donées statiques, applications, base de données).
* Gestion des paramêtres d'environnement.

----

### Orchestration des taches d'installation

* Installation en annule et remplace.
* Installation cumulative (ex bdd).

----

### Tactique de déploiement

* Multiplier les déploiements les rend plus simples
* Version de migration, compatible avec la version -1 et la cible
* Déploiement sans arrêt de service.

----

### Conclusion déploiement continu

Les déploiements doivent être une opération simple et quotidienne.

---

## Présentation de différentes solutions CI/CD OpenSource

[Liste sur le site Cloud Native Computing Foundation](https://landscape.cncf.io/category=continuous-integration-delivery&format=card-mode&grouping=no&license=open-source&sort=stars)

----

![maxpic](images/argo.svg)
* https://argoproj.github.io/

Open source Kubernetes native workflows, events, CI and CD

----

![maxpic](images/brigade.svg)
* https://brigade.sh/

Brigade is a tool for running scriptable, automated tasks in the cloud — as part of your Kubernetes cluster.

----

![maxpic](images/buildkite.svg)
* https://buildkite.com/

The Buildkite Agent is an open-source toolkit written in Golang for securely running build jobs on any device or network

----

![maxpic](images/concourse.svg)
* https://concourse-ci.org/

----

![maxpic](images/container-ops.svg)
* https://containerops.org/

----

![maxpic](images/drone.svg)
* https://drone.io

----

![maxpic](images/gitkube.svg)
* https://gitkube.sh/

Build and deploy docker images to Kubernetes using git push

----

![maxpic](images/git-lab.svg)
* https://www.gitlab.com

----

![maxpic](images/go-cd.svg)
* https://www.gocd.org


----

![maxpic](images/habitus.svg)
* https://www.habitus.io/

Habitus is a standalone build flow tool for Docker. It is a command line tool that builds Docker images based on their Dockerfile and a build.yml.

----

![maxpic](images/jenkins.svg)
* https://jenkins.io/

The leading open source automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project.

----

![maxpic](images/spinnaker.svg)
* https://www.spinnaker.io/

Spinnaker is an open source, multi-cloud continuous delivery platform for releasing software changes with high velocity and confidence.

----

![maxpic](images/travis-ci.svg)
* https://travis-ci.org/

----

![maxpic](images/weave-flux.svg)
* https://www.weave.works/oss/flux/

The GitOps Kubernetes operator

----

![maxpic](images/wercker.svg)
* https://cloud.oracle.com/en_US/containers/pipelines

The Wercker CLI can be used to execute pipelines locally for both local development and easy introspection.

----

![maxpic](images/zuul-ci.svg)
* https://zuul-ci.org


---

## Mise en pratique avec Gitlab


---

# FIN


