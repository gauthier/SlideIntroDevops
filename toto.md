# Domaine à aborder absolument
- 


# Idées en vrac

- Le logiciel mange le monde
- Release management, sortir des nouvelles versions, avec le moins de bug possible et sans interruption. plus personne ne veut voir une période d'interruption pour maintenance. Les applications doivent être dispo 24/7
- Avec le déploiement continue le code est déployé dès qu'il est prêt
- Le code doit toujours être prêt pour le déploiement. Les tests de qualité doivent être automatique.
- Le gartner dit qu'en 2020, 50% des applications seront déployé automatiquement contre 15% aujourd'hui.
- Le changement de context est très couteux, si un dev travaille sur plusieurs projet, ou s'il doit revenir sur une vieille version de son code  (car la production a des mois de retard sur son dev) il doit réaprendre se replonger dans son code. Il devient totalement improductif.
- Un bug qui est détecté tout de suite est très facile et très rapide à corriger.
- Il faut tester chaque commit et plus les commits sont petits, plus il est facile de comprendre le changement. Commiter au minimum une fois par jour.
- Le déploiement en production doit être le plus rapide possible pour avoir le retour utilisateurs le plus vite possible.
- Il faut avoir la Culture du changement.
- l'intégration et le déploiement continue nécessite la connaissance de nouveaux outils.
- Il faut passer d'une chaine de test de plusieurs jours, heures, à un test qui ne prend que quelques secondes ou minutes.

